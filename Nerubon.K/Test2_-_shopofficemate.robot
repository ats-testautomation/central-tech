*** Settings ***
Library          Selenium2Library
Library          BuiltIn
Suite Setup      Suite Setup 
Suite Teardown   Close All Browsers

*** Variables ***
${browser}      gc
${URL}          https://www.officemate.co.th/en
${pageTitle}    ออฟฟิศเมท (OfficeMate) ที่เดียวครบ ตอบโจทย์ทุกธุรกิจ
${xpath_Quantity}     //*[@id="txt-AddToCartQty-OFM8015945"]
${timeout}      20s
${Categories}    /office-supplies/calculator-tools

*** Keywords ***
Suite Setup
   Open Browser  ${URL}  ${browser}
   Maximize Browser Window

Scroll Page To Location
    [Arguments]    ${x_location}    ${y_location}
    Execute JavaScript    window.scrollTo(${x_location},${y_location})

Find the product with the lowest price
    [Arguments]        ${x_brand}      ${x_sort_by}
    Go To     ${URL}${Categories}
    Wait Until Location Is    ${URL}${Categories}
    Go To     ${URL}${Categories}?ofm_brand_name=${x_brand}
    Wait Until Location Is    ${URL}${Categories}?ofm_brand_name=${x_brand}
    Go To     ${URL}${Categories}?ofm_brand_name=${x_brand}&ofm_category_id=892&ofm_sort=${x_sort_by}
    Wait Until Location Is     ${URL}${Categories}?ofm_brand_name=${x_brand}&ofm_category_id=892&ofm_sort=${x_sort_by}
    Scroll Page To Location    0    150
    Wait Until Page Contains     Canon LS-88Hi III Calculator      ${timeout}
    Sleep   3s
    #Capture Page Screenshot      product sort by price asc-{index}.png
    
 
Order Shopping cart
    [Arguments]    ${x_Quantity}
    Wait Until Element Is Visible    ${xpath_Quantity}    10s
    Input Text       ${xpath_Quantity}      ${x_Quantity}
    Wait Until Element Is Visible     xpath=//*[@id="btn-addCart-OFM8015945"]   10s
    Sleep   4s
    Click Element      xpath=//*[@id="btn-addCart-OFM8015945"]
    Sleep   4s
    #Capture Page Screenshot      Add To Cart-{index}.png
    
*** Test Cases ***
canon calculator with the lowest price 2 items  
    
    #1.Open website
    Title Should Be	     ${pageTitle}
    
    #2.Find the canon calculator with the lowest price
    Find the product with the lowest price       Canon       price_asc
    
    #3.Add 2 lowest price calculators into cart
    Order Shopping cart      2      #Quantity
   
    #4.Open cart page and verify whether there are 2 items in cart
    
    Wait Until Element Is Visible     xpath=//*[@id="lbl-minicartQty"]    10s
    Click Element      xpath=//*[@id="lbl-minicartQty"]
    Go To      https://www.officemate.co.th/en/cart
    Sleep   5s
    ${x_incart}    Get Value      xpath=//*[@id="txt-AddToCartQty-OFM8015945"]
    Should Be True      '${x_incart}' == '2'
    Sleep   3s
    Capture Page Screenshot    View cart-{index}.png
    



    
